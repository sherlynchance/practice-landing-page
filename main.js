const toggleOpen = () => {
  let isOpen = true;
  const mainContent = document.querySelector('.main-content');
  const notificationPanel = document.getElementById('notification-panel');
  if (isOpen) {
    notificationPanel.classList.add('close-notif');
    mainContent.style.marginTop = 0;
    isOpen = false;
  }
};

const notificationButton = document.getElementById('notification-button');
notificationButton.addEventListener('click', toggleOpen);

const newsletterPanel = document.querySelector('.newsletter-panel');
const closePanel = () => {
  newsletterPanel.classList.remove('panel-open');
};

const scrollFunction = () => {
  if (localStorage.getItem('ttl') !== null) {
    closePanel();
  } else if (document.body.scrollTop > 150 || document.documentElement.scrollTop > 150) {
    newsletterPanel.classList.add('panel-open');
  }
};

window.onscroll = () => scrollFunction();

const setTtl = (key) => {
  const currTime = new Date().getTime();
  const ttl = 600000;
  const state = currTime + ttl;
  localStorage.setItem(key, JSON.stringify(state));
};

const getTtl = (key) => {
  const ttl = JSON.parse(localStorage.getItem(key));
  const currTime = new Date().getTime();
  if (ttl < currTime) {
    localStorage.removeItem(key);
  }
};

const closePanelBtn = document.getElementById('close-panel');
closePanelBtn.addEventListener('click', () => {
  setTtl('ttl');
})

document.body.onscroll = () => {
  getTtl('ttl');
  scrollFunction();
};
